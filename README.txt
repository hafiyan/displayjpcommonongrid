This repository contains a static function called "DisplayJPCommonOnGrid" within a class.
This function displays the content of JPCommonTree (https://bitbucket.org/hafiyan/jpcommontree) inside a Grid control in WPF.
This function inserts many controls into the Grid to display the accent information contained within JPCommonTree. It also sets the events called by the controls inserted so the Grid can be used to edit the accent information.

This code is distributed with no license nor warranty.
Hafiyan Prafianto
Sendai, November 27th 2014
