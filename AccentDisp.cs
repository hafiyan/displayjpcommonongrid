﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Input;
using JPCommonTree;

namespace AccentCorrection1
{
    public class AccentDisp
    {
        /// <summary>
        /// Converts a Grid to display the content of JPCommonTree, whose accent information can then be corrected.
        /// </summary>
        /// <param name="tree">JPCommonTree to be displayed.
        /// The information regarding word should be discarded beforehand.</param>
        /// <param name="grid">The Grid to be converted.</param>
        public static void DisplayJPCommonOnGrid(JPCommonTree.JPCommonTree tree, Grid grid)
        {
            int total_morae;
            int i;

            ColumnDefinition col_def;
            RowDefinition row_def;

            Mora m;

            //Adding rows to the grid
            for (i = 0; i < 2; i++)
            {
                row_def = new RowDefinition();
                row_def.Height = GridLength.Auto;

                grid.RowDefinitions.Add(row_def);
            }

            //Adding columns to the grid
            //Adding header column
            col_def = new ColumnDefinition();
            col_def.Width = GridLength.Auto;
            grid.ColumnDefinitions.Add(col_def);
            //Putting text inside the header
            PutUIElementInGrid(CreateTextBlock("アクセント"), grid, 0, 0);
            PutUIElementInGrid(CreateTextBlock("発音"), grid, 1, 0);

            //Adding columns containing morae
            i = 0;
            for (m = tree.MoraHead; m != null; m = m.Next)
            {
                //Column for accent correction
                col_def = new ColumnDefinition();
                col_def.Width = new GridLength(5);
                grid.ColumnDefinitions.Add(col_def);

                //Column containing mora
                col_def = new ColumnDefinition();
                col_def.Width = GridLength.Auto;
                grid.ColumnDefinitions.Add(col_def);
                PutUIElementInGrid(CreateTextBlock(m.MoraName), grid, 1, GetMoraColumn(i));
                
                i++;
            }
            //Adding last column
            col_def = new ColumnDefinition();
            col_def.Width = new GridLength(5);
            grid.ColumnDefinitions.Add(col_def);

            //Preparing to add accent correction controls
            total_morae = i;
            grid.UpdateLayout();

            //Filling the inactive columns
            Rectangle r;
            r = new Rectangle(); r.Width = 5; r.Height = grid.ActualHeight; r.Fill = Brushes.Black;
            PutUIElementInGrid(r, grid, 0, 1);
            Grid.SetRowSpan(r, 2);

            r = new Rectangle(); r.Width = 5; r.Height = grid.ActualHeight; r.Fill = Brushes.Black;
            PutUIElementInGrid(r, grid, 0, GetSeparatorColumn(total_morae - 1));
            Grid.SetRowSpan(r, 2);

            //Putting separators between morae that can be used to merge/create accent phrases
            for (i = 0; i < total_morae - 1; i++)
            {
                r = new Rectangle();
                r.Width = 5; r.Height = grid.RowDefinitions[0].ActualHeight; r.Fill = Brushes.White;
                r.MouseEnter += new MouseEventHandler(ShowBorder);
                r.MouseLeave += new MouseEventHandler(HideBorder);
                r.MouseLeftButtonDown += new MouseButtonEventHandler(AccentSepClicked);
                PutUIElementInGrid(r, grid, 0, GetSeparatorColumn(i));
            }

            //Putting separators between morae that can be used to merge/create breath groups
            for (i = 0; i < total_morae - 1; i++)
            {
                r = new Rectangle();
                r.Width = 5; r.Height = grid.RowDefinitions[1].ActualHeight; r.Fill = Brushes.White;
                r.MouseEnter += new MouseEventHandler(ShowBorder);
                r.MouseLeave += new MouseEventHandler(HideBorder);
                r.MouseLeftButtonDown += new MouseButtonEventHandler(PauseSepClicked);
                PutUIElementInGrid(r, grid, 1, GetSeparatorColumn(i));
            }

            //Putting accent type control
            for (i = 0; i < total_morae; i++)
            {
                r = new Rectangle();
                r.MouseEnter += new MouseEventHandler(ShowBorder);
                r.MouseLeave += new MouseEventHandler(HideBorder);
                r.MouseLeftButtonDown += new MouseButtonEventHandler(AccentTypeIndClicked);
                r.Width = grid.ColumnDefinitions[GetMoraColumn(i)].ActualWidth;
                r.Height = grid.RowDefinitions[0].ActualHeight; r.Fill = Brushes.White;
                PutUIElementInGrid(r, grid, 0, GetMoraColumn(i));
            }

            //Saving the tree within the grid
            grid.Tag = tree;

            //Displaying the visual of accent information
            DisplayVisual(grid);
        }

        private static void AccentSepClicked(object sender, MouseButtonEventArgs e)
        {
            int mora_id = (Grid.GetColumn(sender as UIElement) - 3) / 2;
            Grid grid = (sender as Rectangle).Parent as Grid;
            JPCommonTree.JPCommonTree tree = grid.Tag as JPCommonTree.JPCommonTree;

            int state = GetSeparatorState(grid, mora_id);
            int i;

            int accent_id = 0;
            switch (state)
            {
                case 0:
                    //if this is not an accent phrase separator
                    //then split this mora and the next
                    tree.SplitMoraAndNext(mora_id);
                    break;
                case 1:
                    //if this is an accent phrase separator
                    //then merge accent phrase
                    for (i = 0; i < mora_id; i++)
                    {
                        if(GetSeparatorState(grid, i) > 0) accent_id++;
                    }
                    tree.MergeAccentPhraseWithNext(accent_id);
                    break;
                default:
                    //if this is a breath group separator
                    //then merge breath group
                    //then merge accent phrase
                    int breath_id = 0;
                    for (i = 0; i < mora_id; i++)
                    {
                        if(GetSeparatorState(grid, i) == 2) breath_id++;
                    }
                    tree.MergeBreathGroupWithNext(breath_id);
                    for (i = 0; i < mora_id; i++)
                    {
                        if(GetSeparatorState(grid, i) > 0) accent_id++;
                    }
                    tree.MergeAccentPhraseWithNext(accent_id);
                    break;
            }
            DisplayVisual(grid);
        }

        private static void PauseSepClicked(object sender, MouseButtonEventArgs e)
        {
            int mora_id = (Grid.GetColumn(sender as UIElement) - 3) / 2;
            Grid grid = (sender as Rectangle).Parent as Grid;
            JPCommonTree.JPCommonTree tree = grid.Tag as JPCommonTree.JPCommonTree;

            int state = GetSeparatorState(grid, mora_id);
            int i;

            int accent_id = 0;
            switch (state)
            {
                case 0:
                    //if this is not an accent phrase separator
                    //then split this mora and the next
                    //then insert pause
                    tree.SplitMoraAndNext(mora_id);
                    for (i = 0; i < mora_id; i++)
                    {
                        if (GetSeparatorState(grid, i) > 0) accent_id++;
                    }
                    tree.SplitAccentPhraseAndNext(accent_id);
                    break;
                case 1:
                    //if this is an accent phrase separator
                    //then insert pause
                    for (i = 0; i < mora_id; i++)
                    {
                        if (GetSeparatorState(grid, i) > 0) accent_id++;
                    }
                    tree.SplitAccentPhraseAndNext(accent_id);
                    break;
                default:
                    //if this is a breath group separator
                    //then merge breath group
                    int breath_id = 0;
                    for (i = 0; i < mora_id; i++)
                    {
                        if (GetSeparatorState(grid, i) == 2) breath_id++;
                    }
                    tree.MergeBreathGroupWithNext(breath_id);
                    break;
            }
            DisplayVisual(grid);
        }

        private static void AccentTypeIndClicked(object sender, MouseButtonEventArgs e)
        {
            int mora_id = (Grid.GetColumn(sender as UIElement) - 2) / 2;
            Grid grid = (sender as Rectangle).Parent as Grid;
            JPCommonTree.JPCommonTree tree = grid.Tag as JPCommonTree.JPCommonTree;
            int accent_id = -1;
            int mora_pos = -1;
            int i;

            for (i = 0; i <= mora_id; i++)
            {
                if (GetSeparatorState(grid, i - 1) > 0)
                {
                    accent_id++;
                    mora_pos = -1;
                }
                mora_pos++;
            }

            AccentPhrase a = tree.AccentHead;
            for (i = 0; i < accent_id; i++) a = a.Next;

            if (a.Accent == mora_pos + 1) a.Accent = 0;
            else a.Accent = mora_pos + 1;

            DisplayVisual(grid);
        }

        private static void ShowBorder(object sender, MouseEventArgs e)
        {
            (sender as Rectangle).Stroke = Brushes.Red;
            (sender as Rectangle).StrokeThickness = 1;
        }

        private static void HideBorder(object sender, MouseEventArgs e)
        {
            (sender as Rectangle).StrokeThickness = 0;
        }

        //Gets the state of a separator located between morae
        //0: mora separator, 1: accent phrase separator, 2: breath group separator
        private static int GetSeparatorState(Grid grid, int id)
        {
            Rectangle up = GetAccentSeparatorChild(grid, id);
            if (Brush.Equals(up.Fill, Brushes.White)) return 0;

            Rectangle down = GetPauseSeparatorChild(grid, id);
            return Brush.Equals(down.Fill, Brushes.DarkGray) ? 1 : 2;
        }

        /// <summary>
        /// Creates a TextBlock displaying text.
        /// </summary>
        /// <param name="text">The text to be displayed.</param>
        /// <returns>TextBlock control containing text.</returns>
        public static TextBlock CreateTextBlock(string text)
        {
            TextBlock ret = new TextBlock();
            ret.Text = text;

            return ret;
        }

        /// <summary>
        /// Puts a UIElement inside a Grid.
        /// </summary>
        /// <param name="ui">The UIElement to be put.</param>
        /// <param name="grid">The Grid into which the UIElement is put.</param>
        /// <param name="row">The row number.</param>
        /// <param name="col">The column number.</param>
        public static void PutUIElementInGrid(UIElement ui, Grid grid, int row, int col)
        {
            grid.Children.Add(ui);
            Grid.SetRow(ui, row);
            Grid.SetColumn(ui, col);
        }

        /// <summary>
        /// Checks whether a certain mora is pronounced with high pitch,
        /// given the information of the mora position within the accent phrase,
        /// and the length and the accent type of the accent phrase.
        /// </summary>
        /// <param name="mora_pos">The 0-based position of the mora within the accent phrase.</param>
        /// <param name="mora_length">The length of the accent phrase (the number of morae contained).</param>
        /// <param name="accent_type">The accent type of the accent phrase.</param>
        /// <returns>True if the mora is pronounced with high pitch.
        /// False if the mora is pronounced with low pitch, or if the value given is... weird.</returns>
        public static bool IsHighPitched(int mora_pos, int mora_length, int accent_type)
        {
            if (mora_pos >= mora_length) return false;
            if (accent_type == 0)
            {
                return mora_pos > 0;
            }
            else if (accent_type == 1)
            {
                return mora_pos == 0;
            }
            else if (accent_type > 1 && accent_type <= mora_length)
            {
                if (mora_pos == 0) return false;
                else if (mora_pos < accent_type) return true;
                else return false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Resets the visual display of accent information.
        /// </summary>
        /// <param name="grid">The Grid displaying the content of JPCommonTree.</param>
        public static void ResetVisual(Grid grid)
        {
            JPCommonTree.JPCommonTree tree = grid.Tag as JPCommonTree.JPCommonTree;
            int total_morae = tree.MoraTail.GlobalID + 1;

            //all controls within the Grid that have lower indices are for input purpose.
            int vis_begin = 4 * total_morae + 2;
            while (grid.Children.Count >= vis_begin + 1)
            {
                grid.Children.RemoveAt(vis_begin);
            }

            for (int i = 0; i < total_morae - 1; i++)
            {
                GetAccentSeparatorChild(grid, i).Fill = Brushes.White;
                GetPauseSeparatorChild(grid, i).Fill = Brushes.White;
            }
        }

        /// <summary>
        /// Displays the accent information.
        /// </summary>
        /// <param name="grid">The Grid displaying the content of JPCommonTree.</param>
        public static void DisplayVisual(Grid grid)
        {
            int i;

            JPCommonTree.JPCommonTree tree = grid.Tag as JPCommonTree.JPCommonTree;
            int total_morae = tree.MoraTail.GlobalID + 1;

            //reset visual
            ResetVisual(grid);

            //Setting the background color of mora separators
            int[] vis = AccentPauseSepVisibility(grid);
            for (i = 0; i < vis.Length; i++)
            {
                if (vis[i] > 0)
                {
                    GetAccentSeparatorChild(grid, i).Fill = Brushes.Black;
                    GetPauseSeparatorChild(grid, i).Fill = Brushes.DarkGray;
                }
                if (vis[i] > 1) GetPauseSeparatorChild(grid, i).Fill = Brushes.Black;
            }

            //Showing the indicators of accent type
            bool[] show_ind = AccentTypeIndShown(grid);
            for (i = 0; i < show_ind.Length; i++)
            {
                if (show_ind[i])
                {
                    Polygon p = PentagonIndicator();
                    PutUIElementInGrid(p, grid, 0, GetMoraColumn(i));
                    p.Margin = new Thickness(grid.ColumnDefinitions[GetMoraColumn(i)].ActualWidth / 2 - 2.5, 0, 0, 0);
                }
            }

            //Showing the indicators of pitch change
            show_ind = IsHighPitched(grid);
            //displaying the diamonds indicating the pitch
            for (i = 0; i < show_ind.Length; i++)
            {
                Polygon p = DiamondIndicator();
                PutUIElementInGrid(p, grid, 0, GetMoraColumn(i));
                if (show_ind[i])
                    p.Margin = new Thickness(grid.ColumnDefinitions[GetMoraColumn(i)].ActualWidth / 2 - 2.5, 5, 0, 0);
                else
                    p.Margin = new Thickness(grid.ColumnDefinitions[GetMoraColumn(i)].ActualWidth / 2 - 2.5, 
                        grid.RowDefinitions[0].ActualHeight - 5, 0, 0);
            }
            //displaying the lines
            for (i = 0; i < vis.Length; i++)
            {
                if (vis[i] == 0)
                {
                    Line l = new Line();
                    l.Stroke = Brushes.LightGray;

                    PutUIElementInGrid(l, grid, 0, GetMoraColumn(i));
                    Grid.SetColumnSpan(l, 3);

                    l.X1 = grid.ColumnDefinitions[GetMoraColumn(i)].ActualWidth / 2;
                    l.X2 = grid.ColumnDefinitions[GetMoraColumn(i)].ActualWidth + grid.ColumnDefinitions[GetMoraColumn(i + 1)].ActualWidth / 2 + 5;

                    if (show_ind[i]) l.Y1 = 7.5;
                    else l.Y1 = grid.RowDefinitions[0].ActualHeight - 2.5;

                    if (show_ind[i + 1]) l.Y2 = 7.5;
                    else l.Y2 = grid.RowDefinitions[0].ActualHeight - 2.5;
                }
            }
        }

        //Creates a pentagon indicating the accent type
        private static Polygon PentagonIndicator()
        {
            Polygon ret = new Polygon();
            ret.Points.Add(new Point(0, 0));
            ret.Points.Add(new Point(4, 0));
            ret.Points.Add(new Point(4, 4));
            ret.Points.Add(new Point(2, 6));
            ret.Points.Add(new Point(0, 4));
            ret.Fill = Brushes.Black;

            return ret;
        }

        //Creates a diamond indicating the accent type
        private static Polygon DiamondIndicator()
        {
            Polygon ret = new Polygon();
            ret.Points.Add(new Point(2, 0));
            ret.Points.Add(new Point(4, 2));
            ret.Points.Add(new Point(2, 4));
            ret.Points.Add(new Point(0, 2));
            ret.Fill = Brushes.LightGray;

            return ret;
        }

        //0: invisible, 1: accent phrase separator, 2: pause break
        public static int[] AccentPauseSepVisibility(Grid grid)
        {
            JPCommonTree.JPCommonTree tree = grid.Tag as JPCommonTree.JPCommonTree;
            int total_morae = tree.MoraTail.GlobalID + 1;

            int[] ret = new int[total_morae - 1];

            //if id 0 is invisible
            //this means id 0 and id 1 are parts of the same accent phrase
            //if the visibility is 1
            //this means id 0 and id 1 are parts of different accent phrases, but same breath group
            //if the visibility is 2
            //this means id 0 and id 1 are parts of different breath groups
            Mora current = tree.MoraHead;

            for (int i = 0; i < total_morae - 1; i++)
            {
                if (current.Up.Up.Up == current.Next.Up.Up.Up)
                {
                    if (current.Up.Up == current.Next.Up.Up)
                    {
                        ret[i] = 0;
                    }
                    else
                        ret[i] = 1;
                }
                else
                    ret[i] = 2;
                current = current.Next;
            }

            return ret;
        }

        public static bool[] AccentTypeIndShown(Grid grid)
        {
            JPCommonTree.JPCommonTree tree = grid.Tag as JPCommonTree.JPCommonTree;
            int total_accent = tree.AccentTail.GlobalID + 1;
            int total_morae = tree.MoraTail.GlobalID + 1;

            int[] morae_in_a = new int[total_accent];
            int[] accent_type = new int[total_accent];
            bool[] ret = new bool[total_morae];

            int i, j, k;

            AccentPhrase current = tree.AccentHead;
            for (i = 0; i < total_accent; i++)
            {
                accent_type[i] = current.Accent;
                morae_in_a[i] = current.Childs[0].Tail.GlobalID - current.Childs[0].Head.GlobalID + 1;

                current = current.Next;
            }

            k = 0;
            for (i = 0; i < total_accent; i++)
            {
                for (j = 0; j < morae_in_a[i]; j++)
                {
                    if (accent_type[i] > 0) ret[k] = j == accent_type[i] - 1;
                    else ret[k] = false;
                    k++;
                }
            }

            return ret;
        }

        public static bool[] IsHighPitched(Grid grid)
        {
            JPCommonTree.JPCommonTree tree = grid.Tag as JPCommonTree.JPCommonTree;
            int total_accent = tree.AccentTail.GlobalID + 1;
            int total_morae = tree.MoraTail.GlobalID + 1;

            int[] morae_in_a = new int[total_accent];
            int[] accent_type = new int[total_accent];
            bool[] ret = new bool[total_morae];

            int i, j, k;

            AccentPhrase current = tree.AccentHead;
            for (i = 0; i < total_accent; i++)
            {
                accent_type[i] = current.Accent;
                morae_in_a[i] = current.Childs[0].Tail.GlobalID - current.Childs[0].Head.GlobalID + 1;

                current = current.Next;
            }

            k = 0;
            for (i = 0; i < total_accent; i++)
            {
                for (j = 0; j < morae_in_a[i]; j++)
                {
                    ret[k] = IsHighPitched(j, morae_in_a[i], accent_type[i]);
                    k++;
                }
            }

            return ret;
        }

        //Childs:
        //0: accent label
        //1: pron. label
        //2 - n+1: mora label
        //n+2: leftmost bar
        //n+3: rightmost bar
        //n+4 - 2n+2: accent separator
        //2n+3 - 3n+1: pause separator
        //3n+2 - 4n+1: accent type selector
        //4n+2 - : accent visualizer
        //
        //n: total morae

        static TextBlock GetMoraChild(Grid accent_grid, int index)
        {
            return accent_grid.Children[index + 2] as TextBlock;
        }

        static int GetMoraColumn(int id)
        {
            return id * 2 + 2;
        }

        static Rectangle GetAccentSeparatorChild(Grid accent_grid, int index)
        {
            JPCommonTree.JPCommonTree tree = accent_grid.Tag as JPCommonTree.JPCommonTree;
            int total_morae = tree.MoraTail.GlobalID + 1;

            return accent_grid.Children[total_morae + 4 + index] as Rectangle;
        }

        static Rectangle GetPauseSeparatorChild(Grid accent_grid, int index)
        {
            JPCommonTree.JPCommonTree tree = accent_grid.Tag as JPCommonTree.JPCommonTree;
            int total_morae = tree.MoraTail.GlobalID + 1;

            return accent_grid.Children[2 * total_morae + 3 + index] as Rectangle;
        }

        static int GetSeparatorColumn(int id)
        {
            return id * 2 + 3;
        }

        static int GetMoraColumn(JPCommonTree.JPCommonTree tree, int accent, int id)
        {
            AccentPhrase current = tree.AccentHead;
            for (int i = 0; i < accent; i++) current = current.Next;
            return current.Childs[id].GlobalID;
        }

        //Unused functions
        static Border EncloseInsideBorder(UIElement ui)
        {
            return EncloseInsideBorder(ui, 1);
        }

        static Border EncloseInsideBorder(UIElement ui, double width)
        {
            Border ret = new Border();

            ret.Margin = new Thickness(width);
            ret.Child = ui;

            return ret;
        }

    }
}
